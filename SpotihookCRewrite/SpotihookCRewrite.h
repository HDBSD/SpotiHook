// SpotihookCRewrite.h
#include "stdafx.h"
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <iterator>
#include <string>
#include <Windows.h>

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace VolumeMixerhelper;

namespace Spotihook {

	public ref class Spotify
	{
	public:static HANDLE SpotifyHan;

	public:static Process^ GetSpotifyProcess()
	{
		Process^ PotentialSpotify;
		for each (Process^ element in Process::GetProcessesByName("Spotify"))
		{
			if (element->MainWindowHandle != (IntPtr)0)
			{
				PotentialSpotify = element;
				SpotifyHan = (HANDLE)element->MainWindowHandle;
			}
		}
		return PotentialSpotify;
	}

	public:static int HandleTest()
	{
		return (int)SpotifyHan;
	}

	public: static String^ GetPlayingSong()
	{
		Process^ EmptyProc;
		Process^ SpotifyProc = GetSpotifyProcess();
		if (SpotifyProc == EmptyProc)
		{
			return "Spotify Not Running";
		}
		else
		{
			return SpotifyProc->MainWindowTitle;
		}
	}


	public: static Void PlayPause()
	{
		try {
			int spothandle = GetSpotifyProcess()->MainWindowHandle.ToInt32();
			if (spothandle != 0)
			{

				SendMessage((HWND)spothandle, (UINT)0x0319, (WPARAM)0, (long)917504);
			}
		}
		catch (NullReferenceException^)
		{
			Console::WriteLine("Spotify not found");
			Console::ReadLine();
		}
		catch (Exception^e)
		{
			Console::Clear();
			Console::WriteLine("Exception Caught:\n" + e + "\nPress any Key to continue");
			Console::ReadKey();
		}
	}

	public: static Void NextSong()
	{
		try {
			int spothandle = GetSpotifyProcess()->MainWindowHandle.ToInt32();
			if (spothandle != 0)
			{

				SendMessage((HWND)spothandle, (UINT)0x0319, (WPARAM)0, (long)720896);
			}
		}
		catch (NullReferenceException^)
		{
			Console::WriteLine("Spotify not found");
			Console::ReadLine();
		}
		catch (Exception^e)
		{
			Console::Clear();
			Console::WriteLine("Exception Caught:\n" + e + "\nPress any Key to continue");
			Console::ReadKey();
		}
	}

	public: static Void PreviousSong()
	{
		try {
			int spothandle = GetSpotifyProcess()->MainWindowHandle.ToInt32();
			if (spothandle != 0)
			{

				SendMessage((HWND)spothandle, (UINT)0x0319, (WPARAM)0, (long)786432);
			}
		}
		catch (NullReferenceException^)
		{
			Console::WriteLine("Spotify not found");
			Console::ReadLine();
		}
		catch (Exception^e)
		{
			Console::Clear();
			Console::WriteLine("Exception Caught:\n" + e + "\nPress any Key to continue");
			Console::ReadKey();
		}
	}

	public:static void ToggleLocalMute()
	{

		try
		{
			int pid = GetSpotifyProcess()->Id;
			VolumeMixerHelper::SetApplicationMute(pid, !(bool)(VolumeMixerHelper::GetApplicationMute((pid))));
		}
		catch (Exception^) {}
	}

	public:static void SetLocalVol(FLOAT Vol)
	{
		try
		{
			int pid = GetSpotifyProcess()->Id;
			VolumeMixerHelper::SetApplicationVolume(pid, Vol);
		}
		catch (Exception^) {}
	}

	public:static bool GetLocalMuteStatus()
	{
		bool status = false;
		try {
			int pid = GetSpotifyProcess()->Id;
			status = (bool)VolumeMixerHelper::GetApplicationMute(pid);
		}
		catch (Exception^)
		{
		}
		return status;
	}

	public:static float GetLocalVolume()
	{
		float vol = NULL;
		try {
			int pid = GetSpotifyProcess()->Id;
			vol = (float)VolumeMixerHelper::GetApplicationVolume(pid);
		}
		catch (Exception^)
		{
		}
		return vol;
	}



		   //end
	};
}
