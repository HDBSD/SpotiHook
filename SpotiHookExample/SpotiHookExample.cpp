// SpotiHookExample.cpp : main project file.

#include "stdafx.h"

#include <cstdint>


using namespace System;
using namespace Spotihook;

int main(array<System::String ^> ^args)
{
	while (true)
	{
		String^ Title = Spotify::GetPlayingSong();
		Console::Title = Title;
		Console::WriteLine(Title);
		Console::Write(Environment::UserName + ":Spotify> "); String^ UIO = Console::ReadLine()->ToLower();
		if (UIO == "play" | UIO == "pause" | UIO == "stop")
		{
			Spotify::PlayPause();
			
		}
		else if (UIO == "next" | UIO == ">")
		{
			Spotify::NextSong();
		}
		else if (UIO == "tm")
		{
			Spotify::ToggleLocalMute();
		}
		else if (UIO == "=")
		{
			Console::WriteLine("Mute: " + Spotify::GetLocalMuteStatus() + "\nVol: " + Spotify::GetLocalVolume());
			Console::Read();
		}
		else if (UIO->StartsWith("v= "))
		{
			float newVol = float::Parse(UIO->Substring(3, UIO->Length - 3));
			Spotify::SetLocalVol(newVol);
		}
		else if (UIO == "exit")
		{
			bool loopbreaker = true;
			while (loopbreaker)
			{
				Console::Clear();
				Console::WriteLine("Press Y to terminate or N to cancel.");
				ConsoleKeyInfo Keypress;
				Keypress = Console::ReadKey();
				if (Keypress.Key == ConsoleKey::Y)
				{
					System::Environment::Exit(0x1);
				}
				else if (Keypress.Key == ConsoleKey::N)
				{
					loopbreaker = false;
				}
				
			}
		}
		else if (UIO == "test")
		{
			Spotify::GetSpotifyProcess();
			Console::WriteLine(Spotify::HandleTest());
			Console::ReadKey();
		}
		

		System::Threading::Thread::Sleep(50);
		Console::Clear();
	}
    return 0;
}
